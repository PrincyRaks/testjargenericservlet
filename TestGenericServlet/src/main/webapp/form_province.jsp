<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="Model.Province" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Princy
  Date: 17/11/2022
  Time: 09:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Listes Province</h1>

<form action="province-save.do" method="get">
    <p>Id: <input type="number" name="idprovince"></p>
    <p>Chef Lieu: <input type="text" name="chef_lieu"></p>
    <p>Nom du province:<input type="text" name="nom"></p>
    <input type="submit" value="Validate">
</form>
<hr>
<br>
<%
    HashMap<String,Object> hMap = (HashMap<String,Object>) request.getAttribute("hMapDAO");
%>
<table border="1">
    <tr>
        <td>iD</td>
        <td>Chef Lieu</td>
        <td>Province</td>
    </tr>
        <% for (Iterator<Object> list= hMap.values().iterator(); list.hasNext(); ) {
            ArrayList<Province> liste = (ArrayList<Province>) list.next();
            for (Province p: liste) {
        %>
    <tr>
        <td><%= p.getIdprovince() %></td>
        <td><%= p.getChef_lieu() %></td>
        <td><%= p.getNom() %></td>
    </tr>
        <% } }%>
</body>
</html>
