package Model;


import outils.ModelView;
import outils.Url;

import java.util.ArrayList;
import java.util.HashMap;

public class Province {
    private int idprovince;
    private String chef_lieu;
    private String nom;
    static ArrayList<Province> liste = new ArrayList<>();
    static ModelView modelView = new ModelView();
    static HashMap<String,Object> hashMap = new HashMap<>();
    public Province() {
    }

    public Province(int idprovince, String chef_liey, String nom) {
        this.idprovince = idprovince;
        this.chef_lieu = chef_liey;
        this.nom = nom;
    }

    public int getIdprovince() {
        return idprovince;
    }

    public void setIdprovince(int idprovince) {
        this.idprovince = idprovince;
    }

    public String getChef_lieu() {
        return chef_lieu;
    }

    public void setChef_lieu(String chef_lieu) {
        this.chef_lieu = chef_lieu;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    //Liste
    @Url(nameFunction = "province-list")
    public ModelView ListeProvince(){
        Province province = new Province();
        try{
            province.setIdprovince(1);
            province.setChef_lieu("Antananarivo Renivohitra");
            province.setNom("Antananarivo");
            liste.add(province);
            hashMap.put("list-province",liste);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }finally{
            modelView.sethMapDAO(hashMap);
            modelView.setTarget("form_province.jsp");
        }
        System.out.println("LIST IS CALL");
        return modelView;
    }

    //Insert
    @Url(nameFunction = "province-save")
    public ModelView save() {
        Province province = new Province();
        try {
            province.setIdprovince(this.getIdprovince());
            province.setChef_lieu(this.getChef_lieu());
            province.setNom(this.getNom());
            System.out.println("SQL getParameter: "+province.getIdprovince() +province.getChef_lieu() + province.getNom());
            liste.add(province);
            hashMap.put("province-save",liste);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            modelView.sethMapDAO(hashMap);
            modelView.setTarget("result_save.jsp");
        }
        System.out.println("is SAVE!!");
        return modelView;
    }
}

